<?php

namespace App\Services\Soap;

use SoapClient;

class InstanceSoapClient extends Client implements InterfaceInstanceSoap
{
    public static function init(){
        $wsdlUrl = self::getWsdl();
        $soapClientOptions = [
            'stream_context' => self::generateContext(),
            'cache_wsdl'     => WSDL_CACHE_NONE
        ];
        return new SoapClient($wsdlUrl, $soapClientOptions);
    }
}
