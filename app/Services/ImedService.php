<?php

namespace App\Services;

use App\Services\Soap\Client;
use App\Services\Soap\InstanceSoapClient;
use App\Appointment;
use Illuminate\Support\Facades\Log;

class ImedService extends Client
{
    private $uri;
    private $client;
    private $params;
    private $error_msg;
    private $appointment;

    public function __construct()
    {
        $this->uri = config('imed.base_url');
        $this->params = [
            'CodUsuario' => config('imed.CodUsuario'),
            'CodClave' => config('imed.CodClave'),
            'RutCajero' => config('imed.rutCajero'),
        ];
    }

    public function generatePrevoucher()
    {
        try {
            error_log('DEBUG: generate prevoucher');
            $this->appointment = new Appointment;
            $this->appointment->save();
            Log::info('Appointment id: ' . $this->appointment->id);
            error_log('DEBUG: Appointment id ' . $this->appointment->id);

            $service_name = '/wsgenprebono';

            $this->params['OrigenLlamado'] = 'I';
            $this->params['CanalGen'] = 'P';
            $this->params['TipEvento'] = 'I';
            $this->params['CodFinanciador'] = '78';
            $this->params['CodLugar'] = '13605';
            $this->params['LisPrestador'] = [
                'RutConvenio' => '0077413290-2',
                'CorrConvenio' => '1',
                'RutTratante' => '0005289242-2',
                'RutSolic' => '0005289242-2',
                'NomSolic' => 'dummy',
                'CodEspecia' => '03670',
                'RutBenef' => '7364007-5',
                'IdAgenda' => $this->appointment->id,
                'FecAgenda' => '28-12-2019',
                'HorAge' => '09:45',
                'LisPrestacion' => [
                    'CodPrestacion' => '0101829',
                    'CodItem' => '0',
                    'Cantidad' => '1',
                    'MtoTotal' => '0'
                ]
            ];
            self::setWsdl($this->uri . $service_name);

            $this->client = InstanceSoapClient::init();
            error_log('DEBUG: Instance Soap client OK');
            $response = $this->client->wsgenprebono($this->params);
            error_log('DEBUG: CodError ' . $response->CodError);
            error_log('DEBUG: GloError ' . $response->GloError);
            return $response;
        } catch (\Exception $e) {
            //dd($e->getMessage());
            $this->error_msg = $e->getMessage();
            return null;
        }
    }

    public function getError()
    {
        return $this->error_msg;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getAppointmentId()
    {
        return $this->appointment->id;
    }
}
