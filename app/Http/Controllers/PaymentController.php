<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\initPayment;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Services\ImedService;

class PaymentController extends Controller
{
    private $client;

    function __construct() {
        $this->client = new Client(['base_uri' => config('medpay.base_url')]);
    }

    public function index()
    {
        $imedConnector = new ImedService();
        error_log('DEBUG: init imed connector');
        try {
          error_log('DEBUG: try generate prevoucher');
            $prevoucher = $imedConnector->generatePrevoucher();

            if (!$prevoucher) {
                return "Error al conectar con servicios imed. <br>"
                    . $imedConnector->getError()
                    . '<br>'
                    . $imedConnector->getParams()['LisPrestador']['IdAgenda'];
            }

            $prevoucher_number = $prevoucher->LisPrebono->NumPrebono;
            $beneficiary_name = substr($prevoucher->NombreBeneficiario, 0, 20);
            $appointment_id = $imedConnector->getAppointmentId();

            return view(
                'welcome',
                compact('prevoucher_number', 'beneficiary_name', 'appointment_id')
            );

        } catch(\Exception $e) {
            return $e->getMessage();
        }

    }

    public function init(initPayment $request)
    {
        $data = [];
        $request->merge([
            'branch_code' => '13605',
            'callback' => url('result'),
            'voucher_type' => 1,
            'contributor_names' => 'David Ruiz'
        ]);
        $params = $request->only([
            'branch_code', 'callback', 'appointment_id', 'voucher_type',
            'voucher_number', 'contributor_names'
        ]);

        foreach ($params as $i => $p) {
            array_push($data, ['name' => $i, 'contents' => $p]);
        }

        // TODO: discounts
        $discounts = [];
        if ($request->has('dalemana')) {
            $d = ['name' => 'Clínica Alemana', 'amount' => 100];
            array_push($discounts, $d);
        }

        foreach ($discounts as $i => $d) {
            array_push($data, ['name' => "discounts[$i][name]", 'contents' => $d['name']]);
            array_push($data, ['name' => "discounts[$i][amount]", 'contents' => $d['amount']]);
        }

        try {
            $this->auth();
            $response = $this->requestSession($data);

        } catch (ClientException $e) {
            $this->auth();
            try {
                $response = $this->requestSession($data);
            } catch (ClientException $e) {
                return "error";
            }
        }

        return redirect($response->data->redirect);
    }

    public function confirm(Request $request)
    {
        $this->auth();
        $access_token = session('access_token', '');
        if (!$request->has('token')) {
            return view('failed');
        }
        try {
            $response = $this->client->request(
                'GET', '/api/vouchers/' . $request->token, [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer ' . $access_token
            ]]);

            $data = json_decode($response->getBody()->getContents());
            $transaction_data = $data->data;
//dd($transaction_data);
            switch ($transaction_data->status->id) {
                case 0:
                    $custom_message = 'El pago se encuentra incompleto';
                    break;
                case 1:
                    $custom_message = 'El pago ha sido completado satisfactoriamente';
                    break;
                case 2:
                    $custom_message = 'El pago ha sido cancelado';
                    break;
                case 3:
                    $custom_message = 'El pago está siendo procesado';
                    break;
                case 4:
                    $custom_message = 'El usuario está escogiendo el método de pago';
                    break;
                case 5:
                    $custom_message = 'El pago ha sido anulado';
                    break;
                case 6:
                    $custom_message = 'La transacción ha sido pagada';
                    break;

                default:
                    $custom_message = 'Estado del pago desconocido';
                    break;
            }
        } catch (ClientException $e) {
            return "error";
        }
        if ($response->getStatusCode() == "200") {
            return view('success', compact('transaction_data', 'custom_message'));
        }
        return view('failed');

    }

    private function auth()
    {
        $response = $this->client->request('POST', '/api/oauth/access_tokens', [
            'headers' => [
                'Accept' => 'application/json'
            ],
            'multipart' => [
                ['name' => 'grant_type', 'contents' => 'client_credentials'],
                ['name' => 'client_id', 'contents' => config('medpay.client_id')],
                ['name' => 'client_secret', 'contents' => config('medpay.client_secret')]
            ]
        ]);
        $response_data = json_decode($response->getBody()->getContents(), false);
        session(['access_token' => $response_data->data->access_token]);
    }

    private function requestSession($data)
    {
        $access_token = session('access_token', '');
        $response = $this->client->request('POST', '/api/sessions', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $access_token
            ],
            'multipart' => $data
        ]);
        $response_data = json_decode($response->getBody()->getContents(), false);
        session(['session_token' => $response_data->data->token]);
        return $response_data;
    }
}
