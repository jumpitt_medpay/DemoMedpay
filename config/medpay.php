<?php

return [
    'base_url' => env('MEDPAY_BASE_URL', 'http://localhost:8000'),
    'client_id' => env('MEDPAY_CLIENT_ID'),
    'client_secret' => env('MEDPAY_CLIENT_SECRET')
];
