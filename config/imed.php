<?php

return [
    'base_url' => 'http://wsprebono3pre.bonoelectronico.cl',
    'CodUsuario' => env('IMED_CODUSER', ''),
    'CodClave' => env('IMED_CODKEY', ''),
    'rutCajero' => '1-9',
];
