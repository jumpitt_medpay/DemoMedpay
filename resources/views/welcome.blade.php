<!doctype html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Medpay</title>

	<!-- Bootstrap core CSS -->
	<link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/medpay-payment.css') }}" rel="stylesheet">
</head>

<body>
  <section class="section-top">
    <div class="container">

    </div>
  </section>

  <section class="main">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">

  		  <div class="card" style="margin-top:60px;">
		    <div class="card-body">
			  <div class="row justify-content-center ">
			    <div class="col-10 progress-content">
          <div class="text-center">
            <img src="{{ asset('img/logo.svg') }}" alt="MedPay">
          </div>
				  <h5 class="text-center">Demo</h5>

				  @if ($errors->any())
				    <div class="alert alert-danger">
				      <ul>
				        @foreach ($errors->all() as $error)
		                  <li>{{ $error }}</li>
				        @endforeach
				      </ul>
				    </div>
				  @endif


          <form action="{{ url('payments') }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="appointment_id" value="{{ $appointment_id }}">
            <input type="hidden" name="voucher_number" value="{{ $prevoucher_number }}">
            <table class="table table-hover">
                <tbody>
                    <tr>
                        <th class="detalle-trans" scope="row">Lugar</th>
                        <td class="total-excedentes">[13605] Clínica Alemana</td>
                    </tr>
                    <tr>
                        <th class="detalle-trans" scope="row">ID de agenda</th>
                        <td class="total-excedentes">{{ $appointment_id }}</td>
                    </tr>
                    <tr>
                        <th class="detalle-trans" scope="row">Número de prebono</th>
                        <td class="total-excedentes">{{ $prevoucher_number }}</td>
                    </tr>
                    <tr>
                        <th class="detalle-trans" scope="row">Nombre Beneficiario</th>
                        <td class="total-excedentes">David Ruiz</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <br>
            <div class="row justify-content-center">
              <div class="col-lg-8">
				        <button type="submit" class="btn btn-primary btn-block"> Pagar con Medpay</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('js/jquery.min.js') }}"></script>
  <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>
