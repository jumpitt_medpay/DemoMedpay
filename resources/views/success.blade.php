<!doctype html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Medpay</title>

	<!-- Bootstrap core CSS -->
	<link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/medpay-payment.css') }}" rel="stylesheet">
</head>

<body>
  <section class="section-top">
    <div class="container">

    </div>
  </section>

  <section class="main">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">

  		  <div class="card" style="margin-top:60px;">
		    <div class="card-body">
			  <div class="row justify-content-center ">
			    <div class="col-9 progress-content">
                <div class="text-center">
                    <img src="{{ asset('img/logo.svg') }}" alt="MedPay">
                </div>
				  <h5 class="text-center">Demo</h5>
          <p class="text-center {{ $transaction_data->status->id == 1 ? 'text-success' : 'text-danger' }}">{{ $custom_message }}</p>
          <table class="table table-hover">
                <tbody>
                @if (!empty($transaction_data->payments))
                    <tr>
                        <th class="detalle-trans" scope="row">Número de Agendamiento</th>
                        <td class="total-excedentes">{{ $transaction_data->appointment_id }}</td>
                    </tr>
                    <tr>
                        <th class="detalle-trans" scope="row">Número de Orden</th>
                        <td class="total-excedentes">{{ $transaction_data->id }}</td>
                    </tr>
                    <tr>
                        <th class="detalle-trans" scope="row">Código de Autorización</th>
                        <td class="total-excedentes">{{ $transaction_data->payments[0]->auth_code }}</td>
                    </tr>
                    <tr>
                        <th class="detalle-trans" scope="row">Tipo de Pago</th>
                        <td class="total-excedentes">{{ $transaction_data->payments[0]->quotas_type->name }}</td>
                    </tr>
                    <tr>
                        <th class="detalle-trans" scope="row">Número de Cuotas</th>
                        <td class="total-excedentes">{{ $transaction_data->payments[0]->quotas_amount }}</td>
                    </tr>
                    <tr>
                          <th class="detalle-trans" scope="row">Monto Transacción</th>
                          <td class="total-excedentes">${{ number_format($transaction_data->claim_info->copayment, 0, ',', '.') }} pesos (CLP)</td>
                    </tr>
                    <tr>
                        <th class="detalle-trans" scope="row">Fecha de Transacción</th>
                        <td class="total-excedentes">{{ date( 'd/m/Y H:i:s', strtotime($transaction_data->created_at)) }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
                  <a href="{{ url('/') }}"><button class="btn btn-primary btn-block">Volver</button>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('js/jquery.min.js') }}"></script>
  <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>
